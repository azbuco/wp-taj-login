<?php

/*
 * login - logout item hozzáadása a főmenühöz
 */
add_filter('wp_nav_menu_items', function ($items, $args) {
    if ($args->theme_location == 'primary') {
        if (is_user_logged_in()) {
            $items .= '<li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    ' . wp_get_current_user()->display_name . ' 
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="' . wp_logout_url() . '">Kilépés</a>
                </div>
 
            </li>';
        } else {
            $items .= '<li class="nav-item"><a class="nav-link" href="' . wp_login_url(get_permalink()) . '">Bejelentkezés</a></li>';
        }
    }
    return $items;
}, 10, 2);


