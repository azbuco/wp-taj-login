=== WP TAJ Login ===
Contributors: azbuco
Requires at least: 4.6
Tested up to: 5.3.2
Stable tag: 1.2.8
Requires PHP: 7.2
License: GPLv2

WordPress bejelentkezés TAJ számmal, a normál bejelentkező dialógusok helyett.

==Description== 

Lecseréli az alap wp bejelentkezési metódust egy egyedi azonosító alapján történő bejelentkezésre, kifelyezetten a dolgozói portálokhoz.

==Installation== 

Töltsd le innen a zip fájlt, másold a plugin mappába "wp-taj-login" néven, engedélyezd és kész.

A frissítések automatikusan történnek bitbucketről

== Changelog ==

= 1.2.8 =

* Instrukciók frissítve
* Nem törli le az alap wp szerepköröket

= 1.2 =

* Frissítés bitbucketről
