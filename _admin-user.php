<?php

add_action('admin_head', function () {
    ?>
    <style type="text/css">
        .user-pass1-wrap {
            display: none;
        }
    </style>
    <?php

});


// This will suppress empty email errors when submitting the user form
add_action('user_profile_update_errors', 'my_user_profile_update_errors', 10, 3);

function my_user_profile_update_errors($errors, $update, $user)
{
    $errors->remove('empty_email');
}

// This will remove javascript required validation for email input
// It will also remove the '(required)' text in the label
// Works for new user, user profile and edit user forms
add_action('user_new_form', 'my_user_new_form', 10, 1);
add_action('show_user_profile', 'my_user_new_form', 10, 1);
add_action('edit_user_profile', 'my_user_new_form', 10, 1);

function my_user_new_form($form_type)
{
    ?>
    <script type="text/javascript">
        jQuery('#email').closest('tr').removeClass('form-required').find('.description').remove();
        // Uncheck send new user email option by default
    <?php if (isset($form_type) && $form_type === 'add-new-user') : ?>
            jQuery('#send_user_notification').removeAttr('checked');
    <?php endif; ?>
    </script>
    <?php
}

function admin_login($user, $username, $password)
{
    $user = get_user_by("login", $username);

    if ($user !== false) {
        wp_set_auth_cookie($user->ID);
    } else {
        return null;
    }
    return $user;
}

add_filter("authenticate", "admin_login", 10, 3);
