<?php

add_action('login_head', function () {
    ?>
    <style type="text/css">
        .user-pass-wrap {
            display: none;
        }

        .login #nav {
            display: none;
        }
    </style>
    <?php

});

add_filter('gettext', function ($translating) {
    $translated = str_ireplace(', vagy Email cím', '', $translating);
    return $translated;
});


//add_filter('login_redirect', function ($url) {
//    var_dump($url);
//    return home_url();
//});


add_action('load-profile.php', function() {
    if (!current_user_can('manage_options'))
        exit(wp_safe_redirect(home_url()));
});


add_action('template_redirect', 'private_content_redirect_to_login', 9);

function private_content_redirect_to_login()
{
    global $wp_query, $wpdb;
    if (is_404()) {
        $private = $wpdb->get_row($wp_query->request);
        $location = wp_login_url($_SERVER["REQUEST_URI"]);
        if ('private' == $private->post_status) {
            wp_safe_redirect($location);
            exit;
        }
    }
}

//add_action('wp', 'redirect_private_page_to_login');
//
//function redirect_private_page_to_login(){
//
//    global $wp_query;
//    
//    
//    $post_type_object = get_post_type_object( $wp_query->query_vars['post_type'] );
//    
//    var_dump($post_type_object);
//    
//    $queried_object = get_queried_object();
//    
//    var_dump($queried_object);
//
//    if ($queried_object->post_status == "private" && !is_user_logged_in()) {
//
//        wp_redirect(home_url('/login?redirect='.get_permalink($queried_object->ID)));
//
//    } 
//}