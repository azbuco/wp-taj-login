<?php

function taj_login_activation()
{
    //remove_role('subscriber');
    //remove_role('editor');
    //remove_role('contributor');
    //remove_role('author');

    add_role('dolgozo', 'Dolgozó', [
        'read' => true,
        'read_private_posts' => true,
        'read_private_pages' => true,
    ]);
}

function taj_login_deactivation()
{
    // inkább ne csináljon semmit hátha véletlen nyomogatja valaki...
    // remove_role('dolgozo');
}

register_activation_hook(TAJ_LOGIN_FILE, 'taj_login_activation');
register_deactivation_hook(TAJ_LOGIN_FILE, 'taj_login_deactivation');
