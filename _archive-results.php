<?php

function show_private_posts_on_homepage($query)
{
    if ($query->is_home() && $query->is_main_query()) {
        $query->set('post_status', array('publish', 'private'));
    }
}

add_action('pre_get_posts', 'show_private_posts_on_homepage');