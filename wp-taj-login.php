<?php

/**
 * Plugin Name: TAJ login
 * Description: Bejelentkezés TAJ kártya száma alapján
 * Author: Székely Tamás
 * Version: 1.2.8
 */

define('TAJ_LOGIN_FILE', __FILE__);

require_once __DIR__ . '/_admin-user.php';
require_once __DIR__ . '/_admin-users.php';
require_once __DIR__ . '/_archive-results.php';
require_once __DIR__ . '/_login-page.php';
require_once __DIR__ . '/_navbar.php';
require_once __DIR__ . '/_remove-admin-bar.php';
require_once __DIR__ . '/_setup-roles.php';

/**
 * Update checker
 */
require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://bitbucket.org/azbuco/wp-taj-login',
	__FILE__,
	'wp-taj-login'
);

//Optional: If you're using a private repository, create an OAuth consumer
//and set the authentication credentials like this:
//Note: For now you need to check "This is a private consumer" when
//creating the consumer to work around #134:
// https://github.com/YahnisElsts/plugin-update-checker/issues/134
//$myUpdateChecker->setAuthentication(array(
//	'consumer_key' => '...',
//	'consumer_secret' => '...',
//));

//Optional: Set the branch that contains the stable release.
//$myUpdateChecker->setBranch('stable-branch-name');
