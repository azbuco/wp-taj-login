<?php

function taj_login_modify_user_table($columns)
{
    $columns = [
        'cb' => '<input type="checkbox" />',
        'username' => 'TAJ szám',
        'display_name' => 'Név',
        'email' => 'Email',
        'registration_date' => 'Regisztráció dátuma',
    ];
    
    return $columns;
}

function taj_login_modify_user_table_row($row_output, $column_id_attr, $user)
{

    $date_format = 'Y.m.d H:i';

    switch ($column_id_attr) {
        case 'registration_date' :
            return date($date_format, strtotime(get_the_author_meta('registered', $user)));
            break;
        case 'display_name':
            return get_userdata($user)->display_name;
            break;
        default:
    }

    return $row_output;
}

function taj_login_make_registered_column_sortable($columns)
{
    return wp_parse_args([
        'registration_date' => 'registered',
        'display_name' => 'display_name',
    ], $columns);
}

function taj_login_replace_users_labels($translating)
{
//    $translated = str_ireplace('Felhasználónév, vagy Email cím', 'TAJ szám', $translating);
    $translated = str_ireplace('Felhasználónév', 'TAJ szám', $translating);
    return $translated;
}

add_filter('manage_users_columns', 'taj_login_modify_user_table');
add_filter('manage_users_custom_column', 'taj_login_modify_user_table_row', 10, 3);
add_filter('manage_users_sortable_columns', 'taj_login_make_registered_column_sortable');
add_filter('gettext', 'taj_login_replace_users_labels');
